<?php
require_once ("vendor/autoload.php");
//http://docs.slimframework.com/

$app = new \Slim\Slim();

$app->get('/', function () {
	echo "Homepage";
});

$app->get('/hello/:name', function ($name) {
	echo "Olá, " . $name;
});

$app->get('/json/', function () {
	echo json_encode(array(
		'date'=>date("Y-m-d H:i:s")
	));
});

$app->run();