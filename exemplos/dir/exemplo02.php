<?php
$images = scandir("images");

$data = array();

foreach ($images as $img){
	if (!in_array($img, array(".",".."))){ //in array faz a busca e não vamos querer o . e nem o ..
		$filename = "images". DIRECTORY_SEPARATOR . $img;

		$info = pathinfo($filename);
		$info["size"] = filesize($filename);
		$info["modified"] = date("d/m/Y à\s\ H:i:s", filemtime($filename));
		$info["url"] = "http://localhost/cursophp7/exemplos/dir/".str_replace("\\", "/", $filename);

		array_push($data, $info);
	}
}

echo json_encode($data);