<?php
$frutas = array( "abacaxi", "laranga", "manga" );

print_r($frutas);

echo "<hr>";
$carros[0][0] = "GM";
$carros[0][1] = "Cobalt";
$carros[0][2] = "Onix";
$carros[0][3] = "Camaro";

$carros[1][0] = "FORD";
$carros[1][1] = "Fiesta";
$carros[1][2] = "Fusion";
$carros[1][3] = "Eco Sport";

//Exibir Camaro
echo "<h4>Exemplo de arrays Bidimensionais</h4>";
echo $carros[0][3];
echo "<h4>Exibir último carro da Ford</h4>";
echo end($carros[1]);

echo "<h4>Exemplo de arrays tridimensionais</h4>";

$pessoas = array();

array_push($pessoas, array(
	'nome'=>'Herbert',
	'idade'=>13,
));

array_push($pessoas, array(
	'nome'=>'Johannes',
	'idade'=>39,
));

array_push($pessoas, array(
	'nome'=>'Jônatas',
	'idade'=>16,
));
print_r($pessoas);
//caso eu queira a segunda pessoa (índice 1)
echo "<br><br>Pegando a segunda pessoa<br>";
print_r($pessoas[1]);
echo "<br><br>Pegando somente o nome da segunda pessoa<br>";
print_r($pessoas[1]['nome']);