<?php

class Pessoa {

	public $nome = "Rasmus Lerdorf";
	protected $idade = 48;
	private $senha = 123456;

	public function verDados() {
		echo $this->nome . "<br/>";
		echo $this->idade . "<br/>";
		echo $this->senha . "<br/>";
	}

}

$objeto = new Pessoa();

//echo $objeto->nome . "<br/>"; //nome exibe por ser públioco mas idade e senha não
$objeto->verDados(); //Exibe pq dei método público em verDados