<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Testando reCaptcha</title>
    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css"
          integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">
</head>
<body>
<div>
<form action="cadastrar.php" method="post" class="pure-form pure-form-stacked">
    <fieldset>
        <legend>Testando reCaptcha</legend>

        <label for="email">E-mail</label>
        <input name="inputEmail" type="email" placeholder="Email">
        <span class="pure-form-message">Este campo é obrigatório.</span>
        <div class="g-recaptcha" data-sitekey="6LdPo1IUAAAAAD37W8d_wAKuB3GCO3SeKaEn-j7I"></div>
        <button type="submit" class="pure-button pure-button-primary">Enviar</button>
    </fieldset>
</form>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html
