<?php
/**
 * Created by PhpStorm.
 * User: Johannes Nogueira
 * Date: 11/04/2018
 * Time: 17:27
 */
$email = $_POST['inputEmail'];

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
	"secret"=>"6LdPo1IUAAAAACtja4qO3vCE4LuT83NmQltrf2d0",
	"response"=>$_POST['g-recaptcha-response'],
	"remoteip"=>$_SERVER['REMOTE_ADDR'] //Pega IP do Usuário
)));

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$recaptcha = json_decode(curl_exec($ch), true);

curl_close($ch);

if ($recaptcha["success"] === true) {
	echo "OK: ".$_POST["inputEmail"];
} else {
	header ("Location: index.php");
}