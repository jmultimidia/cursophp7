<?php
@date_default_timezone_set( 'America/Sao_Paulo' );
$remote_addr                     = null;
$_SERVER['HTTP_CLIENT_IP']       = null;
$_SERVER['HTTP_X_FORWARDED_FOR'] = null;

if ( ! isset( $_SERVER['HTTP_CLIENT_IP'] ) ) {
	$http_client_ip       = $_SERVER['HTTP_CLIENT_IP'];
	$http_x_forwarded_for = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$remote_addr          = $_SERVER['REMOTE_ADDR'];
}

function somar( $a, $b ) {
	return $a + $b;
}

function saudacao() {
	$hr = date( " H " );

	if ( $hr >= 12 && $hr < 18 ) {
		$resp = "Boa tarde";

	} else if ( $hr >= 0 && $hr < 12 ) {
		$resp = "Bom dia";

	} else {
		$resp = "Boa noite";
	}

	return "$resp";
}

//Pegando a URL atual
function UrlAtual() {
	//$dominio = $_SERVER['HTTP_HOST'];
	//$ref 	  = "https://" . $dominio. $_SERVER['REDIRECT_URL'];
	$ref = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

	return $ref;
}

function myIP() {

	global $remote_addr;

	if ( ! empty( $http_client_ip ) ) {
		$ip = $http_client_ip;
		/* VERIFICO SE O ACESSO PARTIU DE UM SERVIDOR PROXY */
	} elseif ( ! empty( $http_x_forwarded_for ) ) {
		$ip = $http_x_forwarded_for;
	} else {
		/* CASO EU NÃO ENCONTRE NAS DUAS OUTRAS MANEIRAS, RECUPERO DA FORMA TRADICIONAL */
		$ip = $remote_addr;
	}

	return "$ip";
}

//FUNÇÃO QUE LIMPA NUMERO DE CELULAR PARA USAR NO WHATSAPP
function limpaCelular( $valor ) {
	$chars = array( "(", ")", " ", "-" );
	$valor = str_replace( $chars, "", $valor );

	return $valor;
}

// Função para validar e-mails
function isMail( $email ) {
	$er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
	if ( preg_match( $er, $email ) ) {
		return true;
	} else {
		return false;
	}
}

//Função para receber diversos argumentos
function recebe() {

	$argumentos = func_get_args();

	return $argumentos;

}

//PROCURAR NO GOOGLE POR PHP 7 NEW FEATURES
//Função nova no php 7 para receber diversos argumentos (neste caso numeros inteiros)
function somaInt( int ... $valores ) {
	return array_sum( $valores );
}

//Função nova no php 7 para receber diversos argumentos (neste caso numeros decimais)
function somaFloat( float ... $valores ) {
	return array_sum( $valores );
}