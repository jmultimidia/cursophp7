<?php
include "exemplo01.php";

echo saudacao()." Você está em: " .UrlAtual()."<br>";
echo "Meu IP é: " .myIP()."<br>";

echo "<br><strong>Função com vários argumentos</strong>";

var_dump((recebe("String de uma Função", 10)));

echo "<br><strong>Testando Função nova do php 7 com vários argumentos (aqui será uma soma)</strong>";
echo "<br>";
echo somaInt(2, 3);
echo "<br>";
echo somaInt(20, 25);
echo "<br>";
echo somaFloat(1.5, 3.2); //como são inteiros ele ignora o que vem depois da vírgula.
echo "<br>";