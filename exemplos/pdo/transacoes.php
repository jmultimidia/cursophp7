<?php

$conn = new PDO("mysql:host=localhost;dbname=dbphp7;","root","123");

$conn->beginTransaction();

$stmt = $conn->prepare("DELETE FROM tb_usuarios WHERE id_usuario = ?");

$id = 2;

$stmt->execute(array($id));

//$conn->rollBack(); Executou o delete mas voltou
$conn->commit(); // Confirmar

echo "Excluído com sucesso!";