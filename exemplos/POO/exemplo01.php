<?php
class Pessoa {

	public $nome; //Atributo [Fora do método escreve normal)

	public function falar(){//Método (Função dentro de uma classe)

		return "O meu nome é ".$this->nome; //$this dentro do método

	}
}

$glaucio = new Pessoa();
$glaucio->nome = "Gláudio Daniel";
$glaucio->falar();

echo $glaucio->falar();