<?php
class Carro {

	private $modelo;
	private $motor;
	private $ano;

	public function getModelo(){

		return $this->modelo;

	}

	public function setModelo( $modelo ) {

		$this->modelo = $modelo;

	}

	/**
	 * @return mixed
	 */
	//public function getMotor():float { //exibe como string
	public function getMotor():float { //exibe com decimal
		return $this->motor;
	}

	/**
	 * @param mixed $motor
	 */
	public function setMotor( $motor ): void {
		$this->motor = $motor;
	}

	/**
	 * @return mixed
	 */
	//public function getAno():int { //exibe como string
	public function getAno():int { //exibe como inteiro
		return $this->ano;
	}

	/**
	 * @param mixed $ano
	 */
	public function setAno( $ano ): void {
		$this->ano = $ano;
	}

	public function exibir(){
		return array(
			"modelo"=>$this->getModelo(),
			"motor"=>$this->getMotor(),
			"ano"=>$this->getAno()
		);
	}

}

$gol = new Carro();
$gol->setModelo("Gol GT");
$gol->setMotor("1.6");
$gol->setAno("2015");

//print_r($gol->exibir());
var_dump($gol->exibir());