<?php

require_once( "vendor/autoload.php" );

use Rain\Tpl;

$config = array(
	"tpl_dir"   => "tpl/",
	"cache_dir" => "cache/",
	"debug"     => false, // set to false to improve the speed
);
Tpl::configure( $config );
// Add PathReplace plugin (necessary to load the CSS with path replace)
Tpl::registerPlugin( new Tpl\Plugin\PathReplace() );
// create the Tpl object
$tpl = new Tpl;
// assign a variable
$tpl->assign( "name", "Meu Primeiro Template" );
$tpl->assign( "resume", "Aprendendo a usar tpl no meu site." );
$tpl->assign( "version", PHP_VERSION );
// assign an array
//$tpl->assign( "week", array( "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ) );
// draw the template
$tpl->draw( "index" );