<?php
/**
 * Created by PhpStorm.
 * User: Johannes Nogueira
 * Date: 04/04/2018
 * Time: 13:02
 */
$nome          = "Johannes";
$sobrenome     = "Nogueira";
$anoNascimento = 1978; //Número fora das aspas ocupa menos memória

//$nomeCompleto = $nome . $sobrenome; //Fica Junto
$nomeCompleto = $nome . " " . $sobrenome; //Separa

// var_dump($nomeCompleto);
echo $nomeCompleto;

unset( $nomeCompleto ); // poderia passar duas variáveis. Ex: unset($nome, $sobrenome)

if ( isset( $nomeCompleto ) ) {
	echo $nomeCompleto;
}
/////////////////////////////////////
echo "<h4>Exemplo simples de array</h4>";
$frutas = array( "abacaxi", "laranga", "manga" );

echo $frutas[2]; //Exibirá Manga

/////////////////////////////////////
echo "<br>";//Exempplo simples de Objeto

$nascimento = new DateTime();
//var_dump($nascimento);
/////////////////////////////////////

$arquivo = fopen( "teste.php", "r" ); //Abrindo arquivo
//var_dump($arquivo);

$nulo = null; //Ocupa menos espaço ideal para colocar antes de trazer algo do bd, ou request ou apagar alguma variável que já foi definida antes
$nulo = ""; //Ocupa mais espaço

//////////////////////////////////// FUNÇÕES

function teste() {
	global $nome;
	echo $nome;
}

function teste2() {
	$nome = "<br>Herbert ";
	echo $nome . "Agora no teste 2<hr>";
}

teste();

teste2();

/////////////////////////////////////////////
$nome = "JMultimídia";
$nome .= " Desenvolvimento Web";
echo "$nome<hr>";

//////////////////////// SOMANDO
$valorTotal = 0;
$valorTotal += 100;
$valorTotal += 25;
$valorTotal -= 5; //Desconto valor numerico
$valorTotal *= .1;//Exibe total do Desconto por porcentagem (.1 = 10%)
echo $valorTotal;
/////////////////////////
$a = null;
$b = null;
$c = 8;
$d = 10;
//var_dump ($a <=> $c); //se é menor, igual ou maior resultando -1, 0 ou 1.
echo "<hr>";
echo $a ?? $b ?? $c ?? $d; //se a for nulo, exibe b que se for nulo exibe c.
///////////////////////////////
echo "<hr>";
$a = 10;
echo $a ++;
echo "<br>";
echo $a;
//////////////////////////////////////////////// EXEMPLO SUBSTITUI
$texto = "Sexo";
$texto = str_replace( "x", "*", "$texto" );
echo "<hr>";
echo $texto;
echo "<br>";
/////////////////////////////////////////////// EXEMPLO POSITION
$frase   = "A repetição é a mãe da retenção.";
$palavra = "mãe";
$q       = strpos( $frase, $palavra ); //contar caracteres até a palavra mãe
//var_dump($q);
$texto  = substr( $frase, 0, $q );//mostrar até repetição ou substituir $q por 17
$texto2 = substr( $frase, $q + strlen( $palavra ), strlen( $frase ) ); // mostrar texto a partir do 17 caractere (contar caracteres das palavras) e exibir até o final.
echo $texto;
echo "<br>";
echo $texto2;
////////////////////////////////////  TESTE DE FUNÇÃO
function somar( $a, $b ) {
	return $a + $b;
}

$resultado = somar( 10, 25 );
echo "<hr>";
echo $resultado;
///////////////////////////////// IF E ELSE em condição simples e rápida
$minhaIdade = 39;
$idadeMaior = 18;
echo "<hr>";
echo ( $minhaIdade < $idadeMaior ) ? "Sou Menor de Idade" : "Sou Maior de Idade";
////////////////// FOR
echo "<hr>";
for ( $i = 0; $i < 10; $i ++ ) {
	echo $i . "<br>";
}
//EXIBINDO DE 5 EM 5
for ( $i = 0; $i < 100; $i += 5 ) {

	if ( $i > 20 && $i < 80 ) {
		continue;
	} //não exibe entre 20 e 80

	if ( $i === 90 ) {
		break;
	} // quando chegar a 90 para de executar

	echo $i . " -";
}
/////////////////////////// EXIBINDO DATAS A PARTIR DO ANO ATUAL PARA MENOS E PARA MAIS
echo "<hr>";

echo "<select>";
for ( $i = date( "Y" ); $i >= date( "Y" ) - 10; $i -- ) {
	echo '<option value="' . $i . '">' . $i . '</option>';
}

echo "</select>";
echo "<select>";
for ( $i = date( "Y" ); $i <= date( "Y" ) + 10; $i ++ ) {
	echo '<option value="' . $i . '">' . $i . '</option>';
}

echo "</select>";

//////////////////////// FOREACH
echo "<hr>";
$meses = array(
	"Janeiro",
	"Fevereiro",
	"Março",
	"Abril",
	"Maio",
	"Junho",
	"Julho",
	"Agosto",
	"Setembro",
	"Outubro",
	"Novembro",
	"Deezembro"
);

//foreach ($meses as $mes){
foreach ( $meses as $index => $mes ) {
	echo "Índice: " . $index . "<br>";
	echo "O mes é: " . $mes . "<br><br>";
}

echo "<h4>Formulário com Foreach</h4>";
?>
    <form method="post">
        <label>Nome:</label><input type="text" name="nome" placeholder="Informe o Nome">
        <label>Email:</label><input type="text" name="email" placeholder="Informe um e-mail">
        <input type="submit" value="Enviar">
    </form>
<?php
if ( isset( $_POST ) ) {

	foreach ( $_POST as $key => $value ) {

		echo "Nome do campo:" . $key . "<br>";
		echo "Valor do campo:" . $value;
		echo "<hr>";

	}

}
///////////////////////////////// SORTEANDO NÚMERO E PARANDO EM UM DETERMINADO (no caso aqui o 7)
$condicao = true;

while ( $condicao ) {
	$numero = rand( 1, 20 );

	if ($numero === 7){
	    echo "SETE!!!";
	    $condicao = false; //para o sorteio
    }
    echo $numero ." ";
}
