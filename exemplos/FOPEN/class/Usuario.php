<?php

class Usuario {

	private $idusuario;
	private $deslogin;
	private $dessenha;
	private $dtcadastro;

	public function getIdusuario() {
		return $this->idusuario;
	}

	public function setIdusuario( $value ) {
		$this->idusuario = $value;
	}

	public function getDeslogin() {
		return $this->deslogin;
	}

	public function setDeslogin( $value ) {
		$this->deslogin = $value;
	}

	public function getDessenha() {
		return $this->dessenha;
	}

	public function setDessenha( $value ) {
		$this->dessenha = $value;
	}

	public function getDtcadastro() {
		return $this->dtcadastro;
	}

	public function setDtcadastro( $value ) {
		$this->dtcadastro = $value;
	}

	public function loadByID( $id ) {

		$sql = new SQL();

		$results = $sql->select( "SELECT * FROM tb_usuarios WHERE id_usuario = :ID", array(
			"ID" => $id
		) );

		if ( count( $results ) > 0 ) {
			/*
			$row = $results[0];

			$this->setIdusuario($row['id_usuario']);
			$this->setDeslogin($row['deslogin']);
			$this->setDessenha($row['dessenha']);
			$this->setDtcadastro(new DateTime($row['dtcadastro']));
			*/
			$this->setData( $results[0] );
		}
	}

	public static function getList() {
		$sql = new Sql();

		return $sql->select( "SELECT * FROM tb_usuarios ORDER BY deslogin" );
	}

	public static function search( $login ) {
		$sql = new Sql();

		return $sql->select( "SELECT * FROM tb_usuarios WHERE deslogin LIKE :SEARCH ORDER BY deslogin", array(
			':SEARCH' => "%" . $login . "%"
		) );
	}

	public function login( $login, $password ) {

		$sql = new SQL();

		$results = $sql->select( "SELECT * FROM tb_usuarios WHERE deslogin = :LOGIN AND dessenha = :PASSWORD", array(
			"LOGIN"    => $login,
			"PASSWORD" => $password
		) );

		if ( count( $results ) > 0 ) {
			$this->setData( $results[0] );
		} else {
			throw new Exception( "Login e/ou senha inválidos." );
		}
	}

	public function setData( $data ) {

		$this->setIdusuario( $data['id_usuario'] );
		$this->setDeslogin( $data['deslogin'] );
		$this->setDessenha( $data['dessenha'] );
		$this->setDtcadastro( new DateTime( $data['dtcadastro'] ) );

	}

	public function insert() {
		$sql     = new Sql();
		$results = $sql->select( "CALL sp_usuarios_insert(:LOGIN, :PASSWORD)", array( //é necessário fazer a procedure no banco de dados
			':LOGIN'    => $this->getDeslogin(),
			':PASSWORD' => $this->getDessenha()
		) );

		if ( count( $results ) > 0 ) {
			$this->setData( $results[0] );
		}
	}

	// EXEMPLO DA PROCEDURE ACIMA NO SQL
	/*
			 DELIMITER $$
		CREATE PROCEDURE sp_usuarios_insert(
		    pdeslogin VARCHAR(50),
		    pdessenha VARCHAR(256)
		)
		BEGIN
		    INSERT INTO tb_usuarios
		        ( deslogin, dessenha)
		    VALUES
		        ( pdeslogin, pdessenha );

		        SELECT * FROM tb_usuarios WHERE id_usuario = LAST_INSERT_ID();
		END$$
	 */

	public function update($login, $password){

		$this->setDeslogin($login);
		$this->setDessenha($password);

		$sql = new Sql();
		$sql->query("UPDATE tb_usuarios SET deslogin = :LOGIN, dessenha = :PASSWORD WHERE id_usuario = :ID", array(
			':LOGIN'=>$this->getDeslogin(),
			':PASSWORD'=>$this->getDessenha(),
			':ID'=>$this->getIdusuario()
		));
	}

	public function delete(){
		$sql = new Sql();
		$sql->query("DELETE FROM tb_usuarios WHERE id_usuario = :ID", array(
			':ID'=>$this->getIdusuario()
		));

		$this->setIdusuario(0);
		$this->setDeslogin("");
		$this->setDessenha("");
		$this->setDtcadastro(new DateTime());
	}

	public function __construct($login = "", $password = "") {
		$this->setDeslogin($login);
		$this->setDessenha($password);
	}

	public function __toString() {
		return json_encode( array(
			"idusuario"  => $this->getIdusuario(),
			"deslogin"   => $this->getDeslogin(),
			"dessenha"   => $this->getDessenha(),
			"dtcadastro" => $this->getDtcadastro()->format( "d/m/Y à\s\ H:i:s" )
		) );
	}
}