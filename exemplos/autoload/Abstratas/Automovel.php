<?php

interface Veiculo {

	public function acelerar( $velocidade );

	public function frenar( $velocidade );

	public function trocarMarca( $velocidade );
}

abstract class Automovel implements Veiculo { //qualquer automóvel implementa a classe veículo mas ninguem pode instanciar automóvel sem dizer o que ele é

	public function acelerar( $velocidade ) {
		echo "O veículo acelerou até " . $velocidade . " Km/h";
	}

	public function frenar( $velocidade ) {
		echo "O veículo frenou até " . $velocidade . " Km/h";
	}

	public function trocarMarca( $velocidade ) {
		echo "O veículo engatou a marcha " . $velocidade;
	}

}