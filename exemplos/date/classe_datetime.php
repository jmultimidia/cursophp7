<?php

$dt = new DateTime();

echo $dt->format("d/m/Y H:i:s");
echo "<br>";

$periodo = new DateInterval("P15D");//Adicionando 15 dias

$dt->add($periodo);
echo "<br>Adicionando 15 dias<br>";
echo $dt->format("d/m/Y H:i:s");
echo "<br>";
