<?php
echo date("d/m/Y");
echo "<br>";
echo date("d/m/Y H:i:s");
echo "<br>";
echo time();
echo "<br>";
echo date("d/m/Y H:i:s", 1523040368);//exibindo data e hora a partir de um timestamp
echo "<br>";

$ts = strtotime("2001-09-11"); //convertendo data em time stamp

echo date("l, d/m/Y", $ts);
echo "<br>";
$ts = strtotime("now"); //convertendo agora em time stamp
echo date("l, d/m/Y", $ts);
echo "<br>";
$ts = strtotime("+1 day"); //adicionando um dia ao de hoje
echo date("l, d/m/Y", $ts);
echo "<br>";
$ts = strtotime("+1 week"); //adicionando uma semana
echo date("l, d/m/Y", $ts);
echo "<br>";
