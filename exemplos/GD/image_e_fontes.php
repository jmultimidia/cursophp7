<?php
/**
 * Created by PhpStorm.
 * User: Johannes Nogueira
 * Date: 10/04/2018
 * Time: 17:51
 */
$image = imagecreatefromjpeg("certificado.jpg");

$titleColor = imagecolorallocate($image, 0,0,0);
$gray = imagecolorallocate($image, 100,100,100); // se repetir um numero é uma variação de cinza

imagettftext($image, 32, 0,320, 250, $titleColor, "fonts". DIRECTORY_SEPARATOR."Bevan-Regular.ttf", "CERTIFICADO");
imagettftext($image, 32,  0,360, 350,  $titleColor, "fonts". DIRECTORY_SEPARATOR."Playball-Regular.ttf", "Johannes Nogueira");
imagestring($image, 3, 440, 370, utf8_decode("Concluído em ").date("d/m/Y"), $titleColor);

header("Content-type: image/jpeg");

//imagejpeg($image);// apenas cria
//imagejpeg($image, "certificado-".date("Y-m-d").".jpg");//criar e salvar no banco de dados com qualidade máxima
imagejpeg($image);
imagedestroy($image);