<?php
/**
 * Created by PhpStorm.
 * User: Johannes Nogueira
 * Date: 10/04/2018
 * Time: 17:38
 */
$image = imagecreatefromjpeg("certificado.jpg");

$titleColor = imagecolorallocate($image, 0,0,0);
$gray = imagecolorallocate($image, 100,100,100); // se repetir um numero é uma variação de cinza

imagestring($image, 5, 400, 150, "CERTIFICADO", $titleColor);
imagestring($image, 5, 440, 350, "Johannes Nogueira", $titleColor);
imagestring($image, 3, 440, 370, utf8_decode("Concluído em ").date("d/m/Y"), $titleColor);

header("Content-type: image/jpeg");

//imagejpeg($image);// apenas cria
imagejpeg($image, "certificado-".date("Y-m-d").".jpg", 85);//criar e salvar no banco de dados
imagedestroy($image);