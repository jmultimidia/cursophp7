<?php
/**
 * Created by PhpStorm.
 * User: Johannes Nogueira
 * Date: 10/04/2018
 * Time: 16:54
 */
header("Content-Type: image/png");

$image = imagecreate(256, 256);
$black = imagecolorallocate($image, 0,0,0); //Preto
$red = imagecolorallocate($image, 255,0,0); //Vermelho

imagestring($image, 5, 60, 120, "Curso PHP7", $red); //5 = Tamanho máximo da fonte, 60 = 60px da margem esquerda, 120px do topo,  $red = cor do texto

imagepng($image);

imagedestroy($image);