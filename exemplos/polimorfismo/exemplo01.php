<?php

abstract class Animal {

	public function falar(){

		return "Som";
	}

	public function mover(){
		return "Anda";
	}
}

class Cachorro extends Animal {

	public function falar() {
		return "Late";
	}

}

class Gato extends Animal {

	public function falar() {
		return "Mia";
	}

}

class Passaros extends Animal {

	public function falar() {
		return "Canta";
	}

	public function mover() {
		return "Voa e " . parent::mover(); // Fazendo polimorfismo cria novo e extende do pai // chamando método mover da classe animal
	}

}

$pluto = new Cachorro();

echo $pluto->falar() . "<br/>"; // Polimorfismo herdando da classe cachorro
echo $pluto->mover() . "<br/><hr>"; // herdando do pai (abstract)

$garfield = new Gato();

echo $garfield->falar() . "<br/>"; // Polimorfismo herdando da classe Gato
echo $garfield->mover() . "<br/><hr>"; // herdando do pai (abstract)

$ave = new Passaros();

echo $ave->falar() . "<br/>";
echo $ave->mover() . "<br/><hr>";