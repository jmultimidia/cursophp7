<?php

interface Veiculo {

	public function acelerar( $velocidade );

	public function frenar( $velocidade );

	public function trocarMarca( $velocidade );
}

class Civic implements Veiculo {

	public function acelerar( $velocidade ) {
		echo "O veículo acelerou até ".$velocidade." Km/h";
	}

	public function frenar( $velocidade ) {
		echo "O veículo frenou até ".$velocidade." Km/h";
	}

	public function trocarMarca( $velocidade ) {
		echo "O veículo engatou a marcha " .$velocidade;
	}

}

$carro = new Civic();
$carro->trocarMarca(1);