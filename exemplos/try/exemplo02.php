<?php
/**
 * Created by PhpStorm.
 * User: Johannes Nogueira
 * Date: 10/04/2018
 * Time: 16:29
 */
function trataNome( $name ) {

	if ( !$name ) {
		throw new Exception( "Nenhum nome foi informado.", 1 );
	}

	echo ucfirst( $name ) . "<br>";
}

try {

	trataNome( "Joao" );
	trataNome( "" );

} catch ( Exception $e ) {

	echo $e->getMessage();

} finally {

	echo "Executou o Try!";

}