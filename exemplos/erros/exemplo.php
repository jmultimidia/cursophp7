<?php
/**
 * Created by PhpStorm.
 * User: Johannes Nogueira
 * Date: 10/04/2018
 * Time: 16:37
 */
function error_handler($code, $message, $file, $line){ //manipular erro

	echo json_encode(array(
		"code"=>$code,
		"message"=>$message,
		"line"=>$line,
		"file"=>$file
	));

}

set_error_handler("error_handler");

$data = 100 / 0; //forçar um erro: Não é possível dividir nada por 0